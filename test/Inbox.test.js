const assert = require("assert");
const mocha = require("mocha");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const { interface, bytecode } = require("../compile");

// Provider acts a as a communication layer
const web3 = new Web3(ganache.provider());

let accounts;
let inbox;
const INITIAL_STRING = 'Hi Audax';
let message;

beforeEach(async () => {
  // Get all accounts
  accounts = await web3.eth.getAccounts();
  
  // Use of the the accounts to deploy your contract onto the Ganache/TestRPC network
  inbox = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode, arguments: [INITIAL_STRING] })
    .send({ from: accounts[3], gas: '1000000' })  
});

describe("Inbox", () => {
  it("deploys a contract", () => {
    assert.ok(inbox.options.address);
  });
  
  it("has a default message", async () => {
    message = await inbox.methods.message().call();
    assert.equal(message, INITIAL_STRING);
  });
  
  it("can set a new message", async () => {
    await inbox.methods.setMessage("Amilly Rock").send({ from: accounts[3] });
    message = await inbox.methods.message().call();
    assert.equal(message, "Amilly Rock");
  })
})